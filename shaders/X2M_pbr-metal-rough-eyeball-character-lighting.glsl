//- Allegorithmic Metal/Rough PBR shader
//- ====================================
//-
//- Import from libraries.
import lib-sss.glsl
import lib-pbr.glsl
import lib-emissive.glsl
import lib-pom.glsl
import lib-utils.glsl

//- Declare the iray mdl material to use with this shader.
//: metadata {
//:   "mdl":"mdl::alg::materials::skin_metallic_roughness::skin_metallic_roughness"
//: }

//- Channels needed for metal/rough workflow are bound here.
//: param auto channel_basecolor
uniform SamplerSparse basecolor_tex;
//: param auto channel_roughness
uniform SamplerSparse roughness_tex;
//: param auto channel_metallic
uniform SamplerSparse metallic_tex;
//: param auto channel_specularlevel
uniform SamplerSparse specularlevel_tex;

//: param custom { "default": false, "label": "X2M Tonemapping" , "group": "Piecewise Power Curves" }
uniform bool x2mCurve;

//: param custom { "default": 1.0, "label": "Toe Strength", "min": 0.0, "max": 10.0 ,"group": "Piecewise Power Curves"}
uniform float toeStrength;

//: param custom { "default": 1.0, "label": "Toe Length", "min": 0.0, "max": 10.0 ,"group": "Piecewise Power Curves"}
uniform float toeLength;

//: param custom { "default": 1.0, "label": "Shoulder Strength", "min": 0.0, "max": 10.0 ,"group": "Piecewise Power Curves"}
uniform float shoulderStrength;

//: param custom { "default": 1.0, "label": "ShoulderLength", "min": 0.0, "max": 10.0 ,"group": "Piecewise Power Curves"}
uniform float shoulderLength;

//: param custom { "default": 1.0, "label": "Shoulder Angle", "min": 0.0, "max": 10.0 ,"group": "Piecewise Power Curves"}
uniform float shoulderAngle; 

//: param custom { "default": 1.0, "label": "Gamma", "min": 0.0, "max": 10.0 ,"group": "Piecewise Power Curves"}
uniform float gamma; 


//: param custom { "default": false, "label": "Eye Light" ,"group": "Eye Light"}
uniform bool viewspaceLightDir;
//: param custom { "default": 1.0, "label": "eye specualr power", "min": 0.0, "max": 5.0 ,"group": "Eye Light"}
uniform float speuclarPower;

//: param auto main_light
uniform vec4 light_main;



vec3 microfacets_brdf( vec3 Nn,	vec3 Ln,vec3 Vn,vec3 Ks,float Roughness)
{
	vec3 Hn = normalize(Vn + Ln);
	float vdh = max( 0.0, dot(Vn, Hn) );
	float ndh = max( 0.0, dot(Nn, Hn) );
	float ndl = max( 0.0, dot(Nn, Ln) );
	float ndv = max( 0.0, dot(Nn, Vn) );
	return fresnel(vdh,Ks) *
		( normal_distrib(ndh,Roughness) * visibility(ndl,ndv,Roughness) / 4.0 );
}



//This code really good but it is related from roughness effect~ not match to this desired result~~~
vec3 viewspacelight_Distribution( vec3 NormalWS,vec3 LightDirWS,	vec3 CameraDirWS, vec3 diffColor , vec3 specColor, float roughness)
{
	return  max(dot(NormalWS,LightDirWS), 0.0) * ( (
		(diffColor*(vec3(1.0,1.0,1.0) - specColor) * M_INV_PI) + microfacets_brdf(NormalWS,LightDirWS,CameraDirWS,	specColor, roughness) ) * M_PI);
}


// X2M Tonemapping

vec3 x2mPiecewiseTonemap(vec3 x)
{
  return x;
}

//- Shader entry point.
void shade(V2F inputs)
{

// Apply parallax occlusion mapping if possible
  vec3 viewTS = worldSpaceToTangentSpace(getEyeVec(inputs.position), inputs);
  applyParallaxOffset(inputs, viewTS);
  
// Fetch material parameters, and conversion to the specular/roughness model
  float roughness = getRoughness(roughness_tex, inputs.sparse_coord);
  vec3 baseColor = getBaseColor(basecolor_tex, inputs.sparse_coord);
  float metallic = getMetallic(metallic_tex, inputs.sparse_coord);
  float specularLevel = getSpecularLevel(specularlevel_tex, inputs.sparse_coord);
  vec3 diffColor = generateDiffuseColor(baseColor, metallic);
  vec3 specColor = generateSpecularColor(specularLevel, baseColor, metallic);
  // Get detail (ambient occlusion) and global (shadow) occlusion factors
  float occlusion = getAO(inputs.sparse_coord) * getShadowFactor();
  float specOcclusion = specularOcclusionCorrection(occlusion, metallic, roughness);
	
	
	//Code block start
	LocalVectors vectors = computeLocalFrame(inputs);	

	//I used very simply method of blinn-phong specular way~
	vec3 normal_vec = computeWSNormal(inputs.tex_coord, inputs.tangent, inputs.bitangent, inputs.normal);
	
	//Very important thing as inputs.position that is from application inputs method
	vec3 viewVector = is_perspective ? normalize(camera_pos - inputs.position) :-camera_dir;
	
	float specular_ndotv = max( 0.0, dot(normal_vec, viewVector) );
	//As you know Substance painter that could not provide to light direction.
	//Generally Substance painter did not has MainLight as Direction lighing kind of game engines's Sun lights~~~.
	//Virtual light vector created via uniform value as vector3 data also already defined at above code line~
	// Say again ~ inputs.position is very important stuff~
	vec3 LightVec = normalize(light_main.xyz * 100.0 - inputs.position);
	float vSpec = pow(specular_ndotv,speuclarPower * 256.0);

	//hm.... Why I defined ndotl?
	//Well... generaly this screen space specular do not affect from shadow now then also not affected from opposite side from directiona lighting.
	//So I made simplle mask....
	//ndotl is really good way for the simple mask for the occlusions with specular ~
	float ndotl =  max( 0.0, dot(normal_vec, LightVec) );
	vSpec = mix(0.0 , vSpec , ndotl);
	//-- Code block end for the screen space simple specular~

	vec3 viewspacelight_SpecularDistribution = mix(vec3(0.0,0.0,0.0) , viewspacelight_Distribution(normal_vec, viewVector, viewVector, vec3(0.0,0.0,0.0),specColor, roughness) , ndotl);

	// Feed parameters for a physically based BRDF integration
	//vec4 color = pbrComputeBRDF(inputs, diffColor, specColor, glossiness, occlusion);



	if(viewspaceLightDir)
	{
		diffColor += (vSpec + viewspacelight_SpecularDistribution );
	}

	
	if (x2mCurve)
	{
		diffColor = x2mPiecewiseTonemap(color);
	}

	emissiveColorOutput(pbrComputeEmissive(emissive_tex, inputs.tex_coord));
	albedoOutput(diffColor.rgb);
	diffuseShadingOutput(occlusion * envIrradiance(vectors.normal));
	specularShadingOutput(specOcclusion * pbrComputeSpecular(vectors, specColor, roughness));
  	sssCoefficientsOutput(getSSSCoefficients(inputs.sparse_coord));
}


