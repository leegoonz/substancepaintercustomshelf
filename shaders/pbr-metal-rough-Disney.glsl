//- Allegorithmic Metal/Rough PBR shader
//- First author this shader code by JPLee
//- leegoonz@163.com
//- What this shader have purpose Which is providing to extra functions such as detail map and clear-coat shading and so on.


import lib-vectors.glsl
import lib-utils.glsl

//- Declare the iray mdl material to use with this shader.
//: metadata {
//:   "mdl":"mdl::alg::materials::skin_metallic_roughness::skin_metallic_roughness"
//: }

//- Show back faces as there may be holes in front faces.
//: state cull_face off


//==========================  lib-random.glsl =============================================//
//- A 2D blue noise texture containing scalar values
//: param auto texture_blue_noise
uniform sampler2D texture_blue_noise;

//- Blue noise texture resolution
const ivec2 texture_blue_noise_size = ivec2(256);


//- Current frame random seed
//: param auto random_seed
uniform int alg_random_seed;

//- Get an uniform random value based on pixel coordinates.
float getBlueNoiseThreshold()
{
  return texture(texture_blue_noise, gl_FragCoord.xy / vec2(texture_blue_noise_size)).x + 0.5 / 65536.0;
}

//- Get an uniform random value based on pixel coordinates and frame id.
float getBlueNoiseThresholdTemporal()
{
  return fract(getBlueNoiseThreshold() + M_GOLDEN_RATIO * alg_random_seed);
}

//- Return the ith number from fibonacci sequence.
float fibonacci1D(int i)
{
  return fract((float(i) + 1.0) * M_GOLDEN_RATIO);
}

//- Return the ith couple from the fibonacci sequence. nbSample is required to get an uniform distribution.
vec2 fibonacci2D(int i, int nbSamples)
{
  return vec2(
    (float(i)+0.5) / float(nbSamples),
    fibonacci1D(i)
  );
}

//- Return the ith couple from the fibonacci sequence. 
//- nbSample is required to get an uniform distribution. This version has a per frame and per pixel pseudo-random rotation applied.
vec2 fibonacci2DDitheredTemporal(int i, int nbSamples)
{
  vec2 s = fibonacci2D(i, nbSamples);
  s.x += getBlueNoiseThresholdTemporal();
  return s;
}

//- lib-sample.glsl
//-Default background colors when there is no data in channel (alpha is 0)
const vec3  DEFAULT_BASE_COLOR       = vec3(0.5);
const float DEFAULT_ROUGHNESS        = 0.3;
const float DEFAULT_METALLIC         = 0.0;
const float DEFAULT_ANISOTROPY_LEVEL = 0.0;
const float DEFAULT_ANISOTROPY_ANGLE = 0.0;
const float DEFAULT_OPACITY          = 1.0;
const float DEFAULT_AO               = 1.0;
const float DEFAULT_SPECULAR_LEVEL   = 0.5;
const float DEFAULT_HEIGHT           = 0.0;
const float DEFAULT_DISPLACEMENT     = 0.0;
const float DEFAULT_SCATTERING       = 0.0;



//============= PBR Disney Body shader code start here ========================//
//- Channels needed for metal/rough workflow are bound here.
//: param auto channel_basecolor
uniform SamplerSparse basecolor_tex;
//: param auto channel_roughness
uniform SamplerSparse roughness_tex;
//: param auto channel_metallic
uniform SamplerSparse metallic_tex;
//: param auto channel_specularlevel
uniform SamplerSparse specularlevel_tex;

//============= Sun Lighting ==================================================//

//: param custom { "default": false, "label": "Sun & Sky" }
uniform bool sun;

//: param custom { "default": 1.0, "label": "Sun Strenght", "min": 0.0, "max": 10.0 }
uniform float sun_strenght;

//: param auto main_light
uniform vec4 light_main;


//- Channels needed for Coat workflow are bound here.
//: param auto channel_user0
uniform SamplerSparse roughnesscoat_tex;

//: param auto channel_user1
uniform SamplerSparse maskcoat_tex;

//: param custom { "group": "Coat Layer" , "label": "Enable", "default": false }
uniform bool coating_Enable;
//: param custom { "group": "Coat Layer", "default": 0.01, "label": "Roughness", "min": 0.0, "max": 1.0 }
uniform float coating_rough;
//: param custom { "group": "Coat Layer", "default": "false", "label": "Override with RoughnessCoat (user0) channel" }
uniform bool coating_rough_use_tex;
//: param custom { "group": "Coat Layer", "default": 1.0, "label": "Opacity", "min": 0.0, "max": 1.0 }
uniform float coating_opacity;
//: param custom { "group": "Coat Layer", "default": "false", "label": "Multiply with MaskCoat (user1) channel" }
uniform bool coating_opacity_use_tex;
//: param custom {
//:   "group": "Coat Layer",
//:   "label": "Surface behavior",
//:   "widget": "combobox",
//:   "default": 1,
//:   "values": {
//:     "Smooth surface": 0,
//:     "Keep details": 1
//:   }
//: }
uniform int coating_surface_behavior;
//: param custom { "group": "Coat Layer", "default": 1.5, "label": "IOR", "min": 1.0, "max": 1.8 }
uniform float coating_ior;

const float DEFAULT_COAT_OPACITY   = 1.0;
const float DEFAULT_COAT_ROUGHNESS = 0.01;

float iorToSpecularLevel(float ior)
{
  float sqrtR0 = (ior-1) / (ior+1);
  return sqrtR0*sqrtR0;
}

float textureSamplerCoat(SamplerSparse sampler, SparseCoord coord, float defaultValue)
{
  vec2 sampledValue = textureSparse(sampler, coord).rg;
  return sampledValue.r + defaultValue * (1.0 - sampledValue.g);
}

float getCoatRoughness(SparseCoord coord)
{
  return coating_rough_use_tex?
    textureSamplerCoat(roughnesscoat_tex, coord, DEFAULT_COAT_ROUGHNESS) :
    coating_rough;
}

float getCoatOpacity(SparseCoord coord)
{
  return coating_opacity_use_tex?
    textureSamplerCoat(maskcoat_tex, coord, DEFAULT_COAT_OPACITY) * coating_opacity :
    coating_opacity;
}

//-AO map
//: param auto ao_blending_mode
uniform int ao_blending_mode;
//: param auto texture_ao
uniform SamplerSparse base_ao_tex;
//: param auto channel_ao
uniform SamplerSparse ao_tex;

//-A value used to tweak the Ambient Occlusion intensity.
//: param custom {
//:   "default": 0.75,
//:   "label": "AO Intensity",
//:   "min": 0.00,
//:   "max": 1.0,
//:   "group": "Common Parameters"
//: }
uniform float ao_intensity;

//-Shadowmask
//: param auto shadow_mask_enable
uniform bool sm_enable;
//: param auto shadow_mask_opacity
uniform float sm_opacity;
//: param auto shadow_mask
uniform sampler2D sm_tex;
//: param auto screen_size
uniform vec4 screen_size;

//-Return sampled glossiness or a default value
float getGlossiness(vec4 sampledValue)
{
  return sampledValue.r + (1.0 - DEFAULT_ROUGHNESS) * (1.0 - sampledValue.g);
}

float getGlossiness(SamplerSparse sampler, SparseCoord coord)
{
  return getGlossiness(textureSparse(sampler, coord));
}

//-Return sampled roughness or a default value
float getRoughness(vec4 sampledValue)
{
  return sampledValue.r + DEFAULT_ROUGHNESS * (1.0 - sampledValue.g);
}

float getRoughness(SamplerSparse sampler, SparseCoord coord)
{
  return getRoughness(textureSparse(sampler, coord));
}

//-Return sampled metallic or a default value
float getMetallic(vec4 sampledValue)
{
  return sampledValue.r + DEFAULT_METALLIC * (1.0 - sampledValue.g);
}

float getMetallic(SamplerSparse sampler, SparseCoord coord)
{
  return getMetallic(textureSparse(sampler, coord));
}

//-Return sampled anisotropy level or a default value
float getAnisotropyLevel(vec4 sampledValue)
{
  return sampledValue.r + DEFAULT_ANISOTROPY_LEVEL * (1.0 - sampledValue.g);
}

float getAnisotropyLevel(SamplerSparse sampler, SparseCoord coord)
{
  return getAnisotropyLevel(textureSparse(sampler, coord));
}


//-Return sampled anisotropy angle or a default value
float getAnisotropyAngle(vec4 sampledValue)
{
  return M_2PI * (sampledValue.r + DEFAULT_ANISOTROPY_ANGLE * (1.0 - sampledValue.g));
}

float getAnisotropyAngle(SamplerSparse sampler, SparseCoord coord)
{
  // Manual trilinear filtering
  float level = max(0.0, textureSparseQueryLod(sampler, coord) + uvtile_lod_bias);
  int level0 = int(level);
  int level1 = level0 + 1;

  ivec2 texSize0 = ivec2(sampler.size.xy) >> level0;
  ivec2 texSize1 = texSize0 >> 1;
  ivec2 itex_coord0 = ivec2(coord.tex_coord * vec2(texSize0));
  ivec2 itex_coord1 = ivec2(coord.tex_coord * vec2(texSize1));

  // Assuming tex sizes are pow of 2, we can do the fast modulo
  ivec2 texSizeMask0 = texSize0 - ivec2(1);
  ivec2 texSizeMask1 = texSize1 - ivec2(1);

  // Fetch the 8 samples needed
  float a000 = getAnisotropyAngle(texelFetch(sampler.tex,  itex_coord0                & texSizeMask0, level0));
  float a001 = getAnisotropyAngle(texelFetch(sampler.tex, (itex_coord0 + ivec2(1, 0)) & texSizeMask0, level0)) - a000;
  float a010 = getAnisotropyAngle(texelFetch(sampler.tex, (itex_coord0 + ivec2(0, 1)) & texSizeMask0, level0)) - a000;
  float a011 = getAnisotropyAngle(texelFetch(sampler.tex, (itex_coord0 + ivec2(1, 1)) & texSizeMask0, level0)) - a000;
  float a100 = getAnisotropyAngle(texelFetch(sampler.tex,  itex_coord1                & texSizeMask1, level1)) - a000;
  float a101 = getAnisotropyAngle(texelFetch(sampler.tex, (itex_coord1 + ivec2(1, 0)) & texSizeMask1, level1)) - a000;
  float a110 = getAnisotropyAngle(texelFetch(sampler.tex, (itex_coord1 + ivec2(0, 1)) & texSizeMask1, level1)) - a000;
  float a111 = getAnisotropyAngle(texelFetch(sampler.tex, (itex_coord1 + ivec2(1, 1)) & texSizeMask1, level1)) - a000;

  // Detect if the angle warps inside the filtering footprint, and fix it
  a001 += abs(a001) > M_PI ? sign(a001) * -M_2PI + a000 : a000;
  a010 += abs(a010) > M_PI ? sign(a010) * -M_2PI + a000 : a000;
  a011 += abs(a011) > M_PI ? sign(a011) * -M_2PI + a000 : a000;
  a100 += abs(a100) > M_PI ? sign(a100) * -M_2PI + a000 : a000;
  a101 += abs(a101) > M_PI ? sign(a101) * -M_2PI + a000 : a000;
  a110 += abs(a110) > M_PI ? sign(a110) * -M_2PI + a000 : a000;
  a111 += abs(a111) > M_PI ? sign(a111) * -M_2PI + a000 : a000;

  // Trilinear blending of the samples
  vec2 t0 = coord.tex_coord * vec2(texSize0) - vec2(itex_coord0);
  vec2 t1 = coord.tex_coord * vec2(texSize1) - vec2(itex_coord1);
  return mix(
    mix(mix(a000, a001, t0.x), mix(a010, a011, t0.x), t0.y),
    mix(mix(a100, a101, t1.x), mix(a110, a111, t1.x), t1.y),
    level - float(level0));
}


//-Return sampled opacity or a default value
float getOpacity(vec4 sampledValue)
{
  return sampledValue.r + DEFAULT_OPACITY * (1.0 - sampledValue.g);
}

float getOpacity(SamplerSparse sampler, SparseCoord coord)
{
  return getOpacity(textureSparse(sampler, coord));
}

//-Return sampled height or a default value
float getHeight(vec4 sampledValue)
{
  return sampledValue.r + DEFAULT_HEIGHT * (1.0 - sampledValue.g);
}

float getHeight(SamplerSparse sampler, SparseCoord coord)
{
  return getHeight(textureSparse(sampler, coord));
}

//-Return sampled displacement or a default value
float getDisplacement(vec4 sampledValue)
{
  return sampledValue.r + DEFAULT_DISPLACEMENT * (1.0 - sampledValue.g);
}

float getDisplacement(SamplerSparse sampler, SparseCoord coord)
{
  return getDisplacement(textureSparse(sampler, coord));
}

//-Return ambient occlusion
float getAO(SparseCoord coord, bool is_premult)
{
  vec2 ao_lookup = textureSparse(base_ao_tex, coord).ra;
  float ao = ao_lookup.x + DEFAULT_AO * (1.0 - ao_lookup.y);

  if (ao_tex.is_set) {
    ao_lookup = textureSparse(ao_tex, coord).rg;
    if (!is_premult) ao_lookup.x *= ao_lookup.y;
    float channel_ao = ao_lookup.x + DEFAULT_AO * (1.0 - ao_lookup.y);
    if (ao_blending_mode == BlendingMode_Replace) {
      ao = channel_ao;
    } else if (ao_blending_mode == BlendingMode_Multiply) {
      ao *= channel_ao;
    }
  }

  // Modulate AO value by AO_intensity
  return mix(1.0, ao, ao_intensity);
}

//-Helper to get ambient occlusion for shading
float getAO(SparseCoord coord)
{
  return getAO(coord, true);
}

//-Return specular level
float getSpecularLevel(vec4 sampledValue)
{
  return sampledValue.r + DEFAULT_SPECULAR_LEVEL * (1.0 - sampledValue.g);
}

float getSpecularLevel(SamplerSparse sampler, SparseCoord coord)
{
  return getSpecularLevel(textureSparse(sampler, coord));
}

//- Fetch the shadowing factor (screen-space)
float getShadowFactor()
{
  float shadowFactor = 1.0;

  if (sm_enable) {
    vec2 screenCoord = (gl_FragCoord.xy * vec2(screen_size.z, screen_size.w));
    vec2 shadowSample = texture(sm_tex, screenCoord).xy;
    // shadowSample.x / shadowSample.y is the normalized shadow factor.
    // shadowSample.x may already be normalized, shadowSample.y contains 0.0 in this case.
    shadowFactor = shadowSample.y == 0.0 ? shadowSample.x : shadowSample.x / shadowSample.y;
  }

  return mix(1.0, shadowFactor, sm_opacity);
}

//-Return sampled base color or a default value
vec3 getBaseColor(vec4 sampledValue)
{
  return sampledValue.rgb + DEFAULT_BASE_COLOR * (1.0 - sampledValue.a);
}

vec3 getBaseColor(SamplerSparse sampler, SparseCoord coord)
{
  return getBaseColor(textureSparse(sampler, coord));
}


//-Return sampled diffuse color or a default value
vec3 getDiffuse(vec4 sampledValue)
{
  return getBaseColor(sampledValue);
}

vec3 getDiffuse(SamplerSparse sampler, SparseCoord coord)
{
  return getDiffuse(textureSparse(sampler, coord));
}


//-Return sampled specular color or a default value
vec3 getSpecularColor(vec4 sampledValue)
{
  vec3 specColor = sampledValue.rgb + DEFAULT_BASE_COLOR * (1.0 - sampledValue.a);
  vec3 defaultF0 = mix(vec3(0.04), specColor, DEFAULT_METALLIC);
  return mix(specColor, defaultF0, (1.0 - sampledValue.a));
}

vec3 getSpecularColor(SamplerSparse sampler, SparseCoord coord)
{
  return getSpecularColor(textureSparse(sampler, coord));
}

//-Generate anisotropic roughness from roughness and anisotropy level
vec2 generateAnisotropicRoughness(float roughness, float anisoLevel)
{
  return vec2(roughness, roughness / sqrt(max(1e-8, 1.0 - anisoLevel)));
}

//-Generate diffuse color from base color and metallic factor
vec3 generateDiffuseColor(vec3 baseColor, float metallic)
{
  return baseColor * (1.0 - metallic);
}

//-Generate specular color from dielectric specular level, base color and metallic factor
vec3 generateSpecularColor(float specularLevel, vec3 baseColor, float metallic)
{
  return mix(vec3(0.08 * specularLevel), baseColor, metallic);
}

//-Generate specular color from base color and metallic factor, using default specular level (0.04) for dielectrics
vec3 generateSpecularColor(vec3 baseColor, float metallic)
{
  return mix(vec3(0.04), baseColor, metallic);
}

//-Return sampled scattering value or a default value
float getScattering(vec4 sampledValue)
{
  return sampledValue.r + DEFAULT_SCATTERING * (1.0 - sampledValue.g);
}

float getScattering(SamplerSparse sampler, SparseCoord coord)
{
  return getScattering(textureSparse(sampler, coord));
}
//- lib-sample.glsl end


//=============================== lib-sss.glsl  ============================================//
//-The scalar SSS coefficient texture

//: param auto channel_scattering
uniform SamplerSparse sss_tex;

//: param auto scene_original_radius
uniform float sssSceneScale;

//: param custom {
//:   "label": "Enable",
//:   "default": false,
//:   "group": "Subsurface Scattering Parameters",
//:   "description": "<html><head/><body><p>Enable the Subsurface Scattering. It needs to be activated in the Display Settings and a Scattering channel needs to be present for these parameters to have an effect.</p></body></html>"
//: }
uniform bool sssEnabled;
//-Select whether the light penetrates straight through the material (translucent) or is diffused before starting to scatter (skin).

//: param custom {
//:   "default": 1,
//:   "label": "Scattering Type",
//:   "widget": "combobox",
//:   "values": {
//:     "Translucent": 0,
//:     "Skin": 1
//:   },
//:   "group": "Subsurface Scattering Parameters",
//:   "description": "<html><head/><body><p>Skin or Translucent/Generic. It needs to be activated in the Display Settings and a Scattering channel needs to be present for these parameters to have an effect.</p></body></html>"
//: }
uniform int sssType;
//-Global scale to the subsurface scattering effect

//: param custom {
//:   "default": 0.5,
//:   "label": "Scale",
//:   "min": 0.01,
//:   "max": 1.0,
//:   "group": "Subsurface Scattering Parameters",
//:   "description": "<html><head/><body><p>Controls the radius/depth of the light absorption in the material. It needs to be activated in the Display Settings and a Scattering channel needs to be present for these parameters to have an effect.</p></body></html>"
//: }
uniform float sssScale;
//-Wavelength dependency of the SSS of the material

//: param custom {
//:   "default": [0.701, 0.301, 0.305],
//:   "label": "Color",
//:   "widget": "color",
//:   "group": "Subsurface Scattering Parameters",
//:   "description": "<html><head/><body><p>The color of light when absorbed by the material. It needs to be activated in the Display Settings and a Scattering channel needs to be present for these parameters to have an effect.</p></body></html>"
//: }
uniform vec3 sssColor;
//-Return the material SSS coefficients

vec4 getSSSCoefficients(float scattering) {
  if (sssEnabled) {
    vec3 sss = sssScale / sssSceneScale * scattering * sssColor;
    return vec4(sss, sss == vec3(0.0) ? 0.0 : 1.0);
  }
  return vec4(0.0);
}
vec4 getSSSCoefficients(SparseCoord coord) {
  if (sssEnabled) {
    return getSSSCoefficients(getScattering(sss_tex, coord));
  }
  return vec4(0.0);
}
//==================================  lib-sss.glsl end  ========================================//




//==================================  lib-pbr.glsl  ============================================//
//-Number of miplevels in the envmap.

//: param auto environment_max_lod
uniform float maxLod;
//-An int representing the number of samples made for specular contribution computation. 
//-The more the higher quality and the performance impact.

//: param custom {
//:   "default": 16,
//:   "label": "Quality",
//:   "widget": "combobox",
//:   "values": {
//:     "Very low (4 spp)": 4,
//:     "Low (16 spp)": 16,
//:     "Medium (32 spp)": 32,
//:     "High (64 spp)": 64,
//:     "Very high (128 spp)": 128,
//:     "Ultra (256 spp)": 256
//:   },
//:   "group": "Common Parameters"
//: }
uniform int nbSamples;

//-Value used to control specular reflection leaking through the surface.

//: param custom {
//:   "default": 1.3,
//:   "label": "Horizon Fading",
//:   "min": 0.0,
//:   "max": 2.0,
//:   "group": "Common Parameters"
//: }
uniform float horizonFade;



//====================================  lib-env.glsl  ==========================================//
//-Engine provided parameters
//: param auto texture_environment
uniform sampler2D environment_texture;
//: param auto environment_rotation
uniform float environment_rotation;
//: param auto environment_exposure
uniform float environment_exposure;
//: param auto environment_irrad_mat_red
uniform mat4 irrad_mat_red;
//: param auto environment_irrad_mat_green
uniform mat4 irrad_mat_green;
//: param auto environment_irrad_mat_blue
uniform mat4 irrad_mat_blue;

//-Helper that allows one to sample environment. 
//-Rotation is taken into account. The environment map is a panoramic env map behind the scene, that's why there is extra computation from dir vector.
vec3 envSampleLOD(vec3 dir, float lod)
{
  // WORKAROUND: Intel GLSL compiler for HD5000 is bugged on OSX:
  // https://bugs.chromium.org/p/chromium/issues/detail?id=308366
  // It is necessary to replace atan(y, -x) by atan(y, -1.0 * x) to force
  // the second parameter to be interpreted as a float
  vec2 pos = M_INV_PI * vec2(atan(-dir.z, -1.0 * dir.x), 2.0 * asin(dir.y));
  pos = 0.5 * pos + vec2(0.5);
  pos.x += environment_rotation;
  return textureLod(environment_texture, pos, lod).rgb * environment_exposure;
}
//-Return the irradiance for a given direction. 
//-The computation is based on environment's spherical harmonics projection.
vec3 envIrradiance(vec3 dir)
{
  float rot = environment_rotation * M_2PI;
  float crot = cos(rot);
  float srot = sin(rot);
  vec4 shDir = vec4(dir.xzy, 1.0);
  shDir = vec4(
    shDir.x * crot - shDir.y * srot,
    shDir.x * srot + shDir.y * crot,
    shDir.z,
    1.0);
  return max(vec3(0.0), vec3(
      dot(shDir, irrad_mat_red * shDir),
      dot(shDir, irrad_mat_green * shDir),
      dot(shDir, irrad_mat_blue * shDir)
    )) * environment_exposure;
}

//==================================  import lib-env.glsl end ====================================//



//-BRDF related functions
const float EPSILON_COEF = 1e-4;

float normal_distrib(
  float ndh,
  float Roughness)
{
  // use GGX / Trowbridge-Reitz, same as Disney and Unreal 4
  // cf http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf p3
  float alpha = Roughness * Roughness;
  float tmp = alpha / max(1e-8,(ndh*ndh*(alpha*alpha-1.0)+1.0));
  return tmp * tmp * M_INV_PI;
}

vec3 fresnel(
  float vdh,
  vec3 F0)
{
  // Schlick with Spherical Gaussian approximation
  // cf http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf p3
  float sphg = exp2((-5.55473*vdh - 6.98316) * vdh);
  return F0 + (vec3(1.0) - F0) * sphg;
}

float G1(
  float ndw, // w is either Ln or Vn
  float k)
{
  // One generic factor of the geometry function divided by ndw
  // NB : We should have k > 0
  return 1.0 / ( ndw*(1.0-k) +  k );
}

float visibility(
  float ndl,
  float ndv,
  float Roughness)
{
  // Schlick with Smith-like choice of k
  // cf http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf p3
  // visibility is a Cook-Torrance geometry function divided by (n.l)*(n.v)
  float k = max(Roughness * Roughness * 0.5, 1e-5);
  return G1(ndl,k)*G1(ndv,k);
}

vec3 cook_torrance_contrib(
  float vdh,
  float ndh,
  float ndl,
  float ndv,
  vec3 Ks,
  float Roughness)
{
  // This is the contribution when using importance sampling with the GGX based
  // sample distribution. This means ct_contrib = ct_brdf / ggx_probability
  return fresnel(vdh,Ks) * (visibility(ndl,ndv,Roughness) * vdh * ndl / ndh );
}

vec3 importanceSampleGGX(vec2 Xi, vec3 T, vec3 B, vec3 N, float roughness)
{
  float a = roughness*roughness;
  float cosT = sqrt((1.0-Xi.y)/(1.0+(a*a-1.0)*Xi.y));
  float sinT = sqrt(1.0-cosT*cosT);
  float phi = 2.0*M_PI*Xi.x;
  return
    T * (sinT*cos(phi)) +
    B * (sinT*sin(phi)) +
    N *  cosT;
}

float probabilityGGX(float ndh, float vdh, float Roughness)
{
  return normal_distrib(ndh, Roughness) * ndh / (4.0*vdh);
}

float distortion(vec3 Wn)
{
  // Computes the inverse of the solid angle of the (differential) pixel in
  // the cube map pointed at by Wn
  float sinT = sqrt(1.0-Wn.y*Wn.y);
  return sinT;
}

float computeLOD(vec3 Ln, float p)
{
  return max(0.0, (maxLod-1.5) - 0.5 * log2(float(nbSamples) * p * distortion(Ln)));
}

//-Horizon fading trick from https://marmosetco.tumblr.com/post/81245981087
float horizonFading(float ndl, float horizonFade)
{
  float horiz = clamp(1.0 + horizonFade * ndl, 0.0, 1.0);
  return horiz * horiz;
}

//-Compute the lambertian diffuse radiance to the viewer's eye
vec3 pbrComputeDiffuse(vec3 normal, vec3 diffColor)
{
  return envIrradiance(normal) * diffColor;
}

//-Compute the microfacets specular reflection to the viewer's eye
vec3 pbrComputeSpecular(LocalVectors vectors, vec3 specColor, float roughness)
{
  vec3 radiance = vec3(0.0);
  float ndv = dot(vectors.eye, vectors.normal);

  for(int i=0; i<nbSamples; ++i)
  {
    vec2 Xi = fibonacci2D(i, nbSamples);
    vec3 Hn = importanceSampleGGX(
      Xi, vectors.tangent, vectors.bitangent, vectors.normal, roughness);
    vec3 Ln = -reflect(vectors.eye,Hn);

    float fade = horizonFading(dot(vectors.vertexNormal, Ln), horizonFade);

    float ndl = dot(vectors.normal, Ln);
    ndl = max( 1e-8, ndl );
    float vdh = max(1e-8, dot(vectors.eye, Hn));
    float ndh = max(1e-8, dot(vectors.normal, Hn));
    float lodS = roughness < 0.01 ? 0.0 : computeLOD(Ln, probabilityGGX(ndh, vdh, roughness));
    radiance += fade * envSampleLOD(Ln, lodS) * cook_torrance_contrib(vdh, ndh, ndl, ndv, specColor, roughness);
  }
  // Remove occlusions on shiny reflections
  radiance /= float(nbSamples);

  return radiance;
}
//-lib-pbr.glsl end

//======================= Extra sun lighting function ==========================================//


//-lib-emissive.glsl
//-The emissive channel texture.
//: param auto channel_emissive
uniform SamplerSparse emissive_tex;

//-A value used to tweak the emissive intensity.
//: param custom {
//:   "default": 1.0,
//:   "label": "Emissive Intensity",
//:   "min": 0.0,
//:   "max": 100.0,
//:   "group": "Common Parameters"
//: }
uniform float emissive_intensity;
//-Compute the emissive radiance to the viewer's eye
vec3 pbrComputeEmissive(SamplerSparse emissive, SparseCoord coord)
{
  return emissive_intensity * textureSparse(emissive, coord).rgb;
}
//-lib-emissive.glsl end


//- Shader entry point.
void shade(V2F inputs)
{
  // Apply parallax occlusion mapping if possible
  vec3 viewTS = worldSpaceToTangentSpace(getEyeVec(inputs.position), inputs);
  // applyParallaxOffset(inputs, viewTS);

  // Fetch material parameters, and conversion to the specular/roughness model
  float roughness = getRoughness(roughness_tex, inputs.sparse_coord);
  vec3 baseColor = getBaseColor(basecolor_tex, inputs.sparse_coord);
  float metallic = getMetallic(metallic_tex, inputs.sparse_coord);
  float specularLevel = getSpecularLevel(specularlevel_tex, inputs.sparse_coord);
  vec3 diffColor = generateDiffuseColor(baseColor, metallic);
  vec3 specColor = generateSpecularColor(specularLevel, baseColor, metallic);
  // Get detail (ambient occlusion) and global (shadow) occlusion factors
  float occlusion = getAO(inputs.sparse_coord) * getShadowFactor();
  float specOcclusion = specularOcclusionCorrection(occlusion, metallic, roughness);

  // Material layer
  LocalVectors vectors = computeLocalFrame(inputs);

  // Feed parameters for a physically based BRDF integration
  albedoOutput(diffColor);
  sssCoefficientsOutput(getSSSCoefficients(inputs.sparse_coord));

  vec3 emissiveColor =  pbrComputeEmissive(emissive_tex, inputs.sparse_coord);
  vec3 diffuseShading = occlusion * envIrradiance(vectors.normal); 
  vec3 specularShading = (specOcclusion * pbrComputeSpecular(vectors, specColor, roughness));


  // Feed parameters for a Clear Coat intergration.
  // Coat layer, specular only
  vec3 wsCoatNormal = coating_surface_behavior == 0?
    tangentSpaceToWorldSpace(vec3(0, 0, 1.0), inputs) :
    computeWSNormal(inputs.sparse_coord, inputs.tangent, inputs.bitangent, inputs.normal);
  LocalVectors coatVectors = computeLocalFrame(inputs, wsCoatNormal, 0.0);
  float coatRoughness = getCoatRoughness(inputs.sparse_coord);
  vec3 coat = specOcclusion * pbrComputeSpecular(coatVectors, vec3(1.0), coatRoughness);

  float coatOpacity = getCoatOpacity(inputs.sparse_coord);
  float blendFactor = coatOpacity * fresnel(max(1e-8, dot(vectors.eye, vectors.normal)), vec3(iorToSpecularLevel(coating_ior))).x;


// Final Output of Shading with Clear coat depend on Toggle of ClearCoat.
if(!coating_Enable){
  emissiveColorOutput( emissiveColor);
  diffuseShadingOutput(diffuseShading);
  specularShadingOutput(specularShading);
}
else{
  emissiveColorOutput(emissiveColor * (1.0 - blendFactor));
  diffuseShadingOutput(diffuseShading * (1.0 - blendFactor));
  specularShadingOutput(mix(specularShading, coat, blendFactor));
}


}
