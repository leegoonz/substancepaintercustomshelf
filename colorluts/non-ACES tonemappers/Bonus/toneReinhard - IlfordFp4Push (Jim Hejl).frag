vec3	tonePFhejl( vec3 s )
{
	// Hejl / PostFilmic_IlfordFp4Push
	const vec3 cb = vec3( 0.0307479, 0.00030400, -0.04458630);
	const vec3 de = vec3(-0.0095000,-0.00162400, -0.01736670);
	const vec3 df = vec3( 0.1493590, 0.21412400,  1.85780000);
	const vec3 ef = vec3(-0.0636051,-0.00758438, -0.00934798);

	const float c = s * s * 1.88;

	// remap color channels
	vec3 ax = vec3(2.36691, 5.14272, 0.49020)*c;
	vec3 pn = (c*(ax+cb)+de);
	vec3 pd = (c*(ax+vec3(0.022,0.004,-0.10543))+df);

	// collapse color channels
	vec3 pr = dot(saturate(pn/pd),0.45);
	return pr;

}

#define	ToneMap	tonePFhejl




