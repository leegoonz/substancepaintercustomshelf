vec3	toneDFHable( vec3 c )
{
	// John Hable Tonemapping curve shaped by Thomas Mansecal
	// TM - Dark Filmic - Hable (Thomas Mansecal) (Blender High)
	const float	A = 0.305, B = 0.055, C = 0.49, D = 0.225, E = 0.04, F = 0.22;
	const float LinWh = 11.2;

	vec3 h = c;
	vec3 TM = ((h*(A*h+vec3(C*B,C*B,C*B))+vec3(D*E,D*E,D*E)) /
			(h*(A*h+vec3(B,B,B))+vec3(D*F,D*F,D*F))) -
			vec3(E/F,E/F,E/F);

	float M = 1.0/(((LinWh*(A*LinWh+C*B)+D*E) /
			(LinWh*(A*LinWh+B)+D*F)) -
			E/F);

	return TM * M;

}

#define	ToneMap	toneDFHable
