vec3	toneBFHable( vec3 c )
{
	// John Hable Tonemapping curve shaped by MJP
	// TM - Bright Filmic - Hable (MJP)
	const float	A = 0.34, B = 0.25, C = 0.1, D = 0.14, E = 0.02, F = 0.24;
	const float LinWh = 11.2;

	vec3 h = c * vec3(2.0,2.0,2.0);
	vec3 TM = ((h*(A*h+vec3(C*B,C*B,C*B))+vec3(D*E,D*E,D*E)) /
			(h*(A*h+vec3(B,B,B))+vec3(D*F,D*F,D*F))) -
			vec3(E/F,E/F,E/F);

	float M = 1.0/(((LinWh*(A*LinWh+C*B)+D*E) /
			(LinWh*(A*LinWh+B)+D*F)) -
			E/F);

	return TM * M;

}

#define	ToneMap	toneBFHable
